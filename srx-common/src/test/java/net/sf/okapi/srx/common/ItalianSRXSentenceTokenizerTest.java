/* LanguageTool, a natural language style checker 
 * Copyright (C) 2014 Daniel Naber (http://www.danielnaber.de)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 */
package net.sf.okapi.srx.common;

import org.junit.Ignore;
import org.junit.Test;

import junit.framework.TestCase;
import net.sf.okapi.srx.languagetool.SRXSentenceTokenizer;
import net.sf.okapi.srx.okapi.OkapiSentenceTokenizer;

public class ItalianSRXSentenceTokenizerTest extends TestCase {

	private final SentenceTokenizer okapiTokenizer = new OkapiSentenceTokenizer("it");
	private final SentenceTokenizer segmentTokenizer = new SRXSentenceTokenizer("it");

	private SentenceTokenizer tokenizer;

	@Test
	public void testOkapiSegmentTest() {
		tokenizer = okapiTokenizer;
		segment();
	}

	@Ignore("Subtle differences with Segmenter SRX engine mean these will fail. Only Test Okapi SRX engine now")
	public void testSegmentTest() {
		tokenizer = segmentTokenizer;
		segment();
	}

	private void segment() {
		testSplit("Il Castello Reale di Racconigi è situato a Racconigi, in provincia di Cuneo ma poco distante da Torino.",
				" Nel corso della sua quasi millenaria storia ha visto numerosi rimaneggiamenti e divenne di proprietà dei Savoia a partire dalla seconda metà del XIV secolo.");
		testSplit("Dott. Bunsen Honeydew"); // abbreviation
		testSplit("Salve Sig.ra Mengoni!", " Come sta oggi?");
		testSplit("Una lettera si può iniziare in questo modo «Il/la sottoscritto/a.».");
		testSplit("La casa costa 170.500.000,00€!");
		testSplit("Salve Sig.ra Mengoni!", " Come sta oggi?");
		testSplit("Buongiorno!", " Sono l'Ing. Mengozzi.", " È presente l'Avv. Cassioni?");
		testSplit("Mi fissi un appuntamento per mar. 23 Nov..", "Grazie.");
		testSplit("Ecco il mio tel.:01234567.", " Mi saluti la Sig.na Manelli.", " Arrivederci.");
		testSplit("La centrale meteor. si è guastata.", " Gli idraul. son dovuti andare a sistemarla.");
		testSplit("Hanno creato un algoritmo allo st. d. arte.", " Si ringrazia lo psicol. Serenti.");
		//testSplit("Chiamate il V.Cte. delle F.P., adesso!");
		testSplit("Giancarlo ha sostenuto l'esame di econ. az..");
		testSplit("Egregio Dir. Amm., le faccio sapere che l'ascensore non funziona.");
		testSplit("Ricordatevi che dom 25 Set. sarà il compleanno di Maria; dovremo darle un regalo.");
		testSplit("La politica è quella della austerità; quindi verranno fatti tagli agli sprechi.");
		testSplit("Nel tribunale, l'Avv. Fabrizi ha urlato \"Io, l'illustrissimo Fabrizi, vi si oppone!\".");
		testSplit("1°C corrisponde a 33.8°F.");
		testSplit("La casa costa 170.500.000,00€!");
		testSplit("Devi comprare : 1)pesce 2)sale.");
	}

	private void testSplit(String... sentences) {
		SrxTestUtils.testSplit(sentences, tokenizer);
	}

}
