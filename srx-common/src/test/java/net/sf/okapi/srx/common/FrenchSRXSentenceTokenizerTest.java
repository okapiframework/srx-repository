/* LanguageTool, a natural language style checker 
 * Copyright (C) 2005 Daniel Naber (http://www.danielnaber.de)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 */
package net.sf.okapi.srx.common;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import net.sf.okapi.srx.languagetool.SRXSentenceTokenizer;
import net.sf.okapi.srx.okapi.OkapiSentenceTokenizer;

public class FrenchSRXSentenceTokenizerTest {

  private final SentenceTokenizer okapiTokenizer = new OkapiSentenceTokenizer("fr");  
  private final SentenceTokenizer segmentTokenizer = new SRXSentenceTokenizer("fr");
    
  private SentenceTokenizer tokenizer;

  @Before
  public void setUp() {
  }
  
  @Test
  public void okapiSegmentTest() {
	  tokenizer = okapiTokenizer;
	  segment();
  }

  @Ignore("Subtle differences with Segmenter SRX engine mean these will fail. Only Test Okapi SRX engine now")
  public void segmentTest() {
	  tokenizer = segmentTokenizer;
	  segment();
  }
  
  // NOTE: sentences here need to end with a space character so they
  // have correct whitespace when appended:
  private void segment() {
	    split("Après avoir été l'un des acteurs du projet génome humain, le Genoscope met aujourd'hui le cap vers la génomique environnementale.", " L'exploitation des données de séquences, prolongée par l'identification expérimentale des fonctions biologiques, notamment dans le domaine de la biocatalyse, ouvrent des perspectives de développements en biotechnologie industrielle.");
	    split("\"Airbus livrera comme prévu 30 appareils 380 cette année avec en ligne de mire l'objectif d'équilibre financier du programme en 2015\", a-t-il ajouté.");
	    split("À 11 heures ce matin, la direction ne décomptait que douze grévistes en tout sur la France : ce sont ceux du site de Saran (Loiret), dont l’effectif est de 809 salariés, dont la moitié d’intérimaires.", " Elle assure que ce mouvement « n’aura aucun impact sur les livraisons ».");
	    split("Son las diez en punto.", " Las siete y media.");
	    split("Ce modèle permet d’afficher le texte « LL.AA.II.RR. » pour l’abréviation de « Leurs Altesses impériales et royales » avec son infobulle.");
	    split("Les derniers ouvrages de Intercept Ltd. sont ici.");	    
  }
  
  private void split(String... sentences) {
	  SrxTestUtils.testSplit(sentences, tokenizer);
  }
  
}
