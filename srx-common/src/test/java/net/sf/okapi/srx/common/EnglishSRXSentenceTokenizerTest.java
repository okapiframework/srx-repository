/* LanguageTool, a natural language style checker 
 * Copyright (C) 2005 Daniel Naber (http://www.danielnaber.de)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 */
package net.sf.okapi.srx.common;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import net.sf.okapi.srx.languagetool.SRXSentenceTokenizer;
import net.sf.okapi.srx.okapi.OkapiSentenceTokenizer;

public class EnglishSRXSentenceTokenizerTest {

  private final SentenceTokenizer okapiTokenizer = new OkapiSentenceTokenizer("en");  
  private final SentenceTokenizer segmentTokenizer = new SRXSentenceTokenizer("en");
    
  private SentenceTokenizer tokenizer;

  @Before
  public void setUp() {
  }
  
  @Test
  public void okapiSegmentTest() {
	  tokenizer = okapiTokenizer;
	  segment();
  }

  @Test //@Ignore("Subtle differences with Segmenter SRX engine mean these will fail. Only Test Okapi SRX engine now")
  public void segmentTest() {
	  tokenizer = segmentTokenizer;
	  segment();
  }
  
  // NOTE: sentences here need to end with a space character so they
  // have correct whitespace when appended:
  private void segment() {
    // incomplete sentences, need to work for on-thy-fly checking of texts:
	split("Ne. 10:17–19");  
    split("Here's a");
    split("Here's a sentence.", " And here's one that's not comp");

    split("This is a sentence. ");
    split("This is a sentence.", " And this is another one.");
    split("This is a sentence.", " Isn't it?", " Yes, it is.");
    split("This is e.g. Mr. Smith, who talks slowly...",
            " But this is another sentence.");
    split("Chanel no. 5 is blah.");
    split("Mrs. Jones gave Peter $4.5, to buy Chanel No 5.",
            " He never came back.");
    split("On p. 6 there's nothing.", " Another sentence.");
    split("Leave me alone!, he yelled.", " Another sentence.");
    split("\"Leave me alone!\", he yelled.");
    split("'Leave me alone!', he yelled.", " Another sentence.");
    split("'Leave me alone!,' he yelled.", " Another sentence.");
    split("This works on the phrase level, i.e. not on the word level.");
    split("Let's meet at 5 p.m. in the main street.");
    split("James comes from the U.K. where he worked as a programmer.");
    split("Don't split strings like U.S.A. please.");
    split("Don't split strings like U. S. A. either.");
    split("Don't split...", " Well you know.", " Here comes more text.");
    split("Don't split... well you know.", " Here comes more text.");
    split("The \".\" should not be a delimiter in quotes.");
    split("\"Here he comes!\", she said.");
    split("\"Here he comes.\"", " But this is another sentence.");
    split("\"Here he comes!\".", " That's what he said.");
    split("The sentence ends here.", " (Another sentence.)");
    split("The sentence (...) ends here.");
    split("The sentence [...] ends here.");
    split("The sentence ends here (...).", " Another sentence.");
    // previously known failed but not now :)
    split("He won't.", " Really.");
    split("He will not.", " Really.");
    split("He won't go.", " Really."); 
    split("He won't say no. 5 is better.", " Not really.");
    split("He won't say No. 5 is better.", " Not really.");
    split("He won't say no.", " Not really.");
    split("He won't say No.", " Not really.");
    split("They met at 5 p.m. on Thursday.");
    split("They met at 5 p.m.", " It was Thursday.");
    split("This is it: a test.");
    split("12) Make sure that the lamp is on.", " 12) Make sure that the lamp is on. ");
    split("He also offers a conversion table (see Cohen, 1988, p. 123). ");
    // one/two returns = paragraph = new sentence:
    split("He won't", "\n\nReally.");
    split("He won't", "\nReally.");
    split("He won't", "\nReally.");
    // Missing space after sentence end:
    split("James is from the Ireland!", " He lives in Spain now.");
    // From the abbreviation list:
    split("Jones Bros. have built a successful company.");
    // parentheses:
    split("It (really!) works.");
    split("It [really!] works.");
    split("It works (really!).", " No doubt.");
    split("It works [really!].", " No doubt.");
    split("It really(!) works well.");
    split("It really[!] works well.");
    split("Translation:", " Rock-bottom prices for local growers.", " \"You're going to lose farmers to bankruptcy; there's no way else to say it,\" says Turp Garrett, the agricultural extension agent for Worcester County, Md.\"", " It's not very pretty.\"");
    split("My name is Jonas E. Smith.");
    split("Please turn to p. 55.");
    split("Were Jane and co. at the party?");
    split("They closed the deal with Pitt, Briggs & Co. at noon.");
    split("Let's ask Jane and co.", " They should know.");
    split("They closed the deal with Pitt, Briggs & Co.", " It closed yesterday.");
    split("I can see Mt. Fuji from here.");
    split("St. Michael's Church is on 5th st. near the light.");
    split("That is JFK Jr.'s book.");
    split("I visited the U.S.A. last year.");
    split("I live in the E.U.", " How about you?");
    split("I live in the U.S.", " How about you?");
    split("I have lived in the U.S. for 20 years.");
    split("At 5 a.m. Mr. Smith went to the bank.", " He left the bank at 6 P.M.",  " They then went to the store.");
    split("She has $100.00.", " It is in her bag.");
    split("He teaches science (He previously worked for 5 years as an engineer.) at the local University.");
    split("Her email is Jane.Doe@example.com.", " I sent her an email.");
    split("The site is: https://www.example.50.com/new-site/awesome_content.html.", " Please check it out.");
    split("She turned to him, 'This is great.' she said.");
    split("She turned to him, \"This is great.\" she said.");
    split("She turned to him, \"This is great.\"", " She held the book out to show him.");
    split("Hello!!", " Long time no see.");
    split("Hello??", " Who is there?");
    split("Hello!?", " Is that you?");
    split("Hello?!", " Is that you?"); 
    split("You can find it at N°. 1026.253.553.", " That is where the treasure is.");
    //split("She works at Yahoo! in the accounting department.");      
    split("The nurse gave him the i.v. in his vein.", " She gave him the i.v.", " It was a great I.V. that she gave him.", " She gave him the I.V.", " It was night.");
    split("Leave me alone!", " He yelled.", " I am in the U.S. Army.", " Charles (Ind.) said he.");
    
    split("The GmbH & Co. KG is a limited partnership with, typically, the sole general partner being a limited liability company.");
    split("\"It's a good thing that the water is really calm,\" I answered ironically.");
    split("December 31, 1988.", " Hello world.", " It's great!", " Born April 05, 1989.");
    split("[A sentence in square brackets.]");
    split("This abbreviation f.e. means for example.");
    split("The med. staff here is very kind.");
    split("What did you order btw., she wondered.");
    split("This is the U.S. Senate my friends.", " Yes.", " It is!");
    split("SEC. 1262 AUTHORIZATION OF APPROPRIATIONS.");
    split("Hello.", " 'This is a test of single quotes.'", " A new sentence.");

    split("1.) The first item.", " 2.) The second item.");    
    split("1) The first item.", " 2) The second item.");
    
    split("1.) The first item 2.) The second item");
    split("1) The first item 2) The second item");
    split("1. The first item.");
    split("a. The first item.");
    split("https://www.example.com/foo/?bar=baz&inga=42&quux");    
  }
  
  private void split(String... sentences) {
	  SrxTestUtils.testSplit(sentences, tokenizer);
  }
  
}
