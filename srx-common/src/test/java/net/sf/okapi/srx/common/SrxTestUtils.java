package net.sf.okapi.srx.common;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class SrxTestUtils {

	public static void testSplit(final String[] sentences, final SentenceTokenizer sTokenizer) {
		final StringBuilder inputString = new StringBuilder();
		final List<String> input = new ArrayList<>();
		Collections.addAll(input, sentences);
		for (final String s : input) {
			inputString.append(s);
		}
		List<String> segments = sTokenizer.tokenize(inputString.toString());
		assertEquals(input, segments);
	}
}
