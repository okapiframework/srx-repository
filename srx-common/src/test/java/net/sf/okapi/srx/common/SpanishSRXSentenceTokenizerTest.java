/* LanguageTool, a natural language style checker 
 * Copyright (C) 2005 Daniel Naber (http://www.danielnaber.de)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 */
package net.sf.okapi.srx.common;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import net.sf.okapi.srx.languagetool.SRXSentenceTokenizer;
import net.sf.okapi.srx.okapi.OkapiSentenceTokenizer;

public class SpanishSRXSentenceTokenizerTest {

  private final SentenceTokenizer okapiTokenizer = new OkapiSentenceTokenizer("es");  
  private final SentenceTokenizer segmentTokenizer = new SRXSentenceTokenizer("es");
    
  private SentenceTokenizer tokenizer;

  @Before
  public void setUp() {
  }
  
  @Test
  public void okapiSegmentTest() {
	  tokenizer = okapiTokenizer;
	  segment();
  }

  @Ignore("Subtle differences with Segmenter SRX engine mean these will fail. Only Test Okapi SRX engine now")
  public void segmentTest() {
	  tokenizer = segmentTokenizer;
	  segment();
  }
  
  // NOTE: sentences here need to end with a space character so they
  // have correct whitespace when appended:
  private void segment() {
	    split("¡Buenas Tardes!");
	    split("¿Y Tú?");
	    split("¡Vaya Ud Derecho!"," ¡Pues Tuerza Ud por la Izquierda/ Derecha!");
	    split("Son las diez en punto.", " Las siete y media.");
	    split("Un área importante de los aeropuertos es el \"centro de control de área\", en el cual se desempeñan los controladores del tráfico aéreo; personas encargadas de dirigir y controlar el movimiento de aeronaves en el aeropuerto y en la zona bajo su jurisdicción.");
	    split("El mayor aeropuerto del mundo es el Aeropuerto Rey Khalid, en Arabia Saudita con un área total de 225 kilómetros cuadrados.");
	    split("He aquí cómo ocurrió:", " En esa época vivía en el Perú un joven príncipe, hermoso y gallardo.");
	    split("La presentación estuvo a cargo del abogado Gabriel Claudio Chamarro y denunció los delitos de \"abuso de autoridad\", \"violación de los deberes de funcionario público\" y \"malversación de caudales públicos\".");
	    split("Pues tuerza Ud. por la S.A. izquierda derecha!");	  
	    split("¿Cómo está hoy?", " Espero que muy bien.");
	    split("¡Hola señorita!", " Espero que muy bien.");
	    split("Hola Srta. Ledesma.", " Buenos días, soy el Lic. Naser Pastoriza, y él es mi padre, el Dr. Naser.");
	    split("¡La casa cuesta $170.500.000,00!", " ¡Muy costosa!", " Se prevé una disminución del 12.5% para el próximo año.");
	    split("«Ninguna mente extraordinaria está exenta de un toque de demencia.», dijo Aristóteles.");
	    split("«Ninguna mente extraordinaria está exenta de un toque de demencia», dijo Aristóteles.", " Pablo, ¿adónde vas?", " ¡¿Qué viste?!");
	    split("Admón. es administración o me equivoco.");
	    split("1. Busca atención prenatal desde el principio");
	    split("¡Hola Srta. Ledesma!",  " ¿Cómo está hoy?", " Espero que muy bien.");
	    split("Buenos días, soy el Lic. Naser Pastoriza, y él es mi padre, el Dr. Naser.");
	    split("He apuntado una cita para la siguiente fecha:", " Mar. 23 de Nov. de 2014.", " Gracias.");
	    split("Núm. de tel: 351.123.465.4.", " Envíe mis saludos a la Sra. Rescia.");
	    split("Cero en la escala Celsius o de grados centígrados (0 °C) se define como el equivalente a 273.15 K, con una diferencia de temperatura de 1 °C equivalente a una diferencia de 1 Kelvin.", " Esto significa que 100 °C, definido como el punto de ebullición del agua, se define como el equivalente a 373.15 K.");
	    split("Durante la primera misión del Discovery (30 Ago. 1984 15:08.10) tuvo lugar el lanzamiento de dos satélites de comunicación, el nombre de esta misión fue STS-41-D.");
	    split("Citando a Criss Jami «Prefiero ser un artista a ser un líder, irónicamente, un líder tiene que seguir las reglas.», lo cual parece muy acertado.");
	    split("Cuando llegué, le estaba dando ejercicios a los niños, uno de los cuales era \"3 + (14/7).x = 5\".", " ¿Qué te parece?");
	    split("Se le pidió a los niños que leyeran los párrf. 5 y 6 del art. 4 de la constitución de los EE. UU..");
	    split("El volumen del cuerpo es 3m³.", " ¿Cuál es la superficie de cada cara del prisma?");
	    split("El corredor No. 103 arrivó 4°.");
	    split("Explora oportunidades de carrera en el área de Salud en el Hospital de Northern en Mt. Kisco.");
	    split("N°. 1026.253.553");
  }
  
  private void split(String... sentences) {
	  SrxTestUtils.testSplit(sentences, tokenizer);
  }
  
}
