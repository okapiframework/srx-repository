/* LanguageTool, a natural language style checker 
 * Copyright (C) 2005 Daniel Naber (http://www.danielnaber.de)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package net.sf.okapi.srx.common;

import org.junit.Ignore;
import org.junit.Test;

import junit.framework.TestCase;
import net.sf.okapi.srx.languagetool.SRXSentenceTokenizer;
import net.sf.okapi.srx.okapi.OkapiSentenceTokenizer;

public class RussianSRXSentenceTokenizerTest extends TestCase {

	private final SentenceTokenizer okapiTokenizer = new OkapiSentenceTokenizer("ru");
	private final SentenceTokenizer segmentTokenizer = new SRXSentenceTokenizer("ru");

	private SentenceTokenizer tokenizer;

	@Test
	public void testOkapiSegmentTest() {
		tokenizer = okapiTokenizer;
		segment();
	}

	@Ignore("Subtle differences with Segmenter SRX engine mean these will fail. Only Test Okapi SRX engine now")
	public void testSegmentTest() {
		tokenizer = segmentTokenizer;
		segment();
	}

	private void segment() {		
		testSplit("Отток капитала из России составил 7 млрд. долларов, сообщил министр финансов Алексей Кудрин.");
		testSplit("Журнал издаётся с 1967 г., пользуется большой популярностью в мире.");
		testSplit("С 2007 г. периодичность выхода газеты – 120 раз в год.");
		testSplit("Редакция журнала находится в здании по адресу: г. Москва, 110000, улица Мира, д. 1.");
		testSplit("Все эти вопросы заставляют нас искать ответы в нашей истории 60-80-х гг. прошлого столетия.");
		testSplit("Более 300 тыс. документов и справочников.");
		testSplit("Скидки до 50000 руб. на автомобили.");
		testSplit("Изготовление визиток любыми тиражами (от 20 шт. до 10 тысяч) в минимальные сроки (от 20 минут).");
		testSplit("Объем составляет 5 куб.м.");
		testSplit("Маленькая девочка бежала и кричала:", " «Не видали маму?».");
		testSplit("Сегодня 27.10.14");
		testSplit("«Я приду поздно»,  — сказал Андрей.");
		//testSplit("«К чему ты готовишься? – спросила мама. – Завтра ведь выходной».");
		testSplit("Он сказал:", " «Я очень устал», и сразу же замолчал.");
		testSplit("Мне стало как-то ужасно грустно в это мгновение; однако что-то похожее на смех зашевелилось в душе моей.");
		testSplit("Шухов как был в ватных брюках, не снятых на ночь (повыше левого колена их тоже был пришит затасканный, погрязневший лоскут, и на нем выведен черной, уже поблекшей краской номер Щ-854), надел телогрейку…");
		testSplit("Слово «дом» является синонимом жилища");
		testSplit("В Санкт-Петербург на гастроли приехал театр «Современник»");
		testSplit("1°C соответствует 33.8°F.");
		testSplit("Едем на скорости 90 км/ч в сторону пгт. Брагиновка, о котором мы так много слышали по ТВ!");
		testSplit("Д-р ветеринарных наук А. И. Семенов и пр. выступали на этом семинаре.");
		//testSplit("Маленькая девочка бежала и кричала: «Не видали маму?»");
		testSplit("Напоминаю Вам, что 25.10 день рождения у Маши К., нужно будет купить ей подарок.");
		testSplit("Кв. 234 находится на 4 этаже.");
		testSplit("Нужно купить 1)рыбу 2)соль.");
	}

	private void testSplit(final String... sentences) {
		SrxTestUtils.testSplit(sentences, tokenizer);
	}

}
