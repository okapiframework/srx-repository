/* LanguageTool, a natural language style checker 
 * Copyright (C) 2014 Daniel Naber (http://www.danielnaber.de)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 */
package net.sf.okapi.srx.common;

import org.junit.Ignore;
import org.junit.Test;

import junit.framework.TestCase;
import net.sf.okapi.srx.languagetool.SRXSentenceTokenizer;
import net.sf.okapi.srx.okapi.OkapiSentenceTokenizer;

public class PortugueseSRXSentenceTokenizerTest extends TestCase {

	private final SentenceTokenizer okapiTokenizer = new OkapiSentenceTokenizer("pt");
	private final SentenceTokenizer segmentTokenizer = new SRXSentenceTokenizer("pt");

	private SentenceTokenizer tokenizer;

	@Test
	public void testOkapiSegmentTest() {
		tokenizer = okapiTokenizer;
		segment();
	}

	@Ignore("Subtle differences with Segmenter SRX engine mean these will fail. Only Test Okapi SRX engine now")
	public void testSegmentTest() {
		tokenizer = segmentTokenizer;
		segment();
	}

	private void segment() {
		// NOTE: sentences here need to end with a space character so they
		// have correct whitespace when appended:
		testSplit("Cola o teu próprio texto aqui.");
		testSplit("Cola o teu próprio texto aqui.", " Ou verifica este texto.");
	}

	private void testSplit(String... sentences) {
		SrxTestUtils.testSplit(sentences, tokenizer);
	}
}
