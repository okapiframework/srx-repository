package net.sf.okapi.srx.okapi;

import java.util.LinkedList;
import java.util.List;

import net.sf.okapi.common.ISegmenter;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.TextUnit;
import net.sf.okapi.lib.segmentation.SRXDocument;
import net.sf.okapi.srx.common.SentenceTokenizer;

public class OkapiSentenceTokenizer implements SentenceTokenizer {
	private static final String DOCUMENT = "/misc/okapi_default.srx";
	
	private ISegmenter segmenter;
	
	public OkapiSentenceTokenizer(final String srxLanguageName) {
		SRXDocument doc = new SRXDocument();
		doc.loadRules(OkapiSentenceTokenizer.class.getResourceAsStream(DOCUMENT));
		// for these tests preserve all whitespace
		doc.setTrimTrailingWhitespaces(false);
		doc.setTrimLeadingWhitespaces(false);
		doc.setUseICU4JBreakRules(true);
		segmenter = doc.compileLanguageRules(new LocaleId(srxLanguageName), null);
	}
	
	@Override
	public List<String> tokenize(String text) {
		List<String> sl = new LinkedList<>();
		ITextUnit tu = new TextUnit("temp", text);
		tu.createSourceSegmentation(segmenter);
		
		for (Segment s : tu.getSource().getSegments()) {
			sl.add(s.text.getText());
		}
		return sl;
	}
}
