<!-- 
LanguageTool is published under the GNU Lesser General Public License (LGPL).
For more detailed license information, see COPYING.txt and README.md.
->

<languagerule languagerulename="Japanese">
<rule break="no">
<beforebreak>[:]+[\p{Pe}\p{Pf}\p{Po}"-[\u002C\u003A\u003B\u055D\u060C\u061B\u0703\u0704\u0705\u0706\u0707\u0708\u0709\u07F8\u1363\u1364\u1365\u1366\u1802\u1804\u1808\u204F\u205D\u3001\uA60D\uFE10\uFE11\uFE13\uFE14\uFE50\uFE51\uFE54\uFE55\uFF0C\uFF1A\uFF1B\uFF64]]*</beforebreak>
<afterbreak>\s+\P{Lu}</afterbreak>
</rule>
<rule break="yes">
<beforebreak>[:]+[\p{Pe}\p{Pf}\p{Po}"-[\u002C\u003A\u003B\u055D\u060C\u061B\u0703\u0704\u0705\u0706\u0707\u0708\u0709\u07F8\u1363\u1364\u1365\u1366\u1802\u1804\u1808\u204F\u205D\u3001\uA60D\uFE10\uFE11\uFE13\uFE14\uFE50\uFE51\uFE54\uFE55\uFF0C\uFF1A\uFF1B\uFF64]]*</beforebreak>
<afterbreak>\s</afterbreak>
</rule>
<rule break="yes">
<beforebreak>[。．！？…]+</beforebreak>
<afterbreak>.</afterbreak>
</rule>
<rule break="no">
<beforebreak>\.\.\.</beforebreak>
<afterbreak>\s+\P{Lu}</afterbreak>
</rule>
<rule break="yes">
<beforebreak>^\s*\p{Nd}+[\p{Nd}\.\)\]]+\s+</beforebreak>
<afterbreak>\p{Lu}</afterbreak>
</rule>
<rule break="yes">
<beforebreak>[\.\?\!]+</beforebreak>
<afterbreak>\s</afterbreak>
</rule>
<rule break="yes">
<beforebreak>[\.!?…][\u00BB\u2019\u201D\u203A"'\p{Pe}\u0002¹²³]*\s</beforebreak>
<afterbreak></afterbreak>
</rule>
<rule break="yes">
<beforebreak>\s\p{L}[\.!?…]\s</beforebreak>
<afterbreak>\p{Lu}\p{Ll}</afterbreak>
</rule>
</languagerule>
