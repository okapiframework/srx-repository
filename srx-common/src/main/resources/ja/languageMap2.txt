<languagerule languagerulename="Japanese">
<rule break="no">
<beforebreak>\p{Nd}*[\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}]+\s*\p{Nd}+[:：]\p{Nd}+(－\p{Nd}+)?。</beforebreak>
<afterbreak>[\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}]</afterbreak>
</rule>
<rule break="no">
<beforebreak>[\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}][。？！](」|』)</beforebreak>
<afterbreak>\s*（\s*[\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}]+\s*\p{Nd}+[:：]\p{Nd}+(ー\p{Nd}+)?</afterbreak>
</rule>
<rule break="no">
<beforebreak>[\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}][。？！]</beforebreak>
<afterbreak>\s*（\s*[\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}]+\s*\p{Nd}+[:：]\p{Nd}+(ー\p{Nd}+)?(参照)?</afterbreak>
</rule>
<rule break="no">
<beforebreak>[\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}][。？！](」|』)?</beforebreak>
<afterbreak>\s*（?\s*[1234１２３４]ニーファイ\p{Nd}+</afterbreak>
</rule>
<rule break="no">
<beforebreak>[\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}][。？！](」|』)</beforebreak>
<afterbreak>\s*（\s*[「『〔]</afterbreak>
</rule>
<rule break="no">
<beforebreak>[\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}][。？！](」|』)</beforebreak>
<afterbreak>と|に</afterbreak>
</rule>
<!--manual section numbers (e.g. 17.2.3 Administering)-->
<rule break="no">
<beforebreak>[0-9]+\.[0-9]+\.</beforebreak>
<afterbreak>[0-9]+\s+\P{Ll}</afterbreak>
</rule>
<!--manual section headings 2-->
<rule break="no">
<beforebreak>[0-9]+\.[0-9]+\.[0-9]+</beforebreak>
<afterbreak>\s+\P{Ll}</afterbreak>
</rule>
<rule break="yes">
<beforebreak>[[\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}]）][。！？]</beforebreak>
<afterbreak>[\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}]</afterbreak>
</rule>
<rule break="yes">
<beforebreak>[[\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}]）][。？！][』|」|）|〕]?</beforebreak>
<afterbreak>\s*[「|『|（]?(\s?……)?[\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}]+</afterbreak>
</rule>
<rule break="yes">
<beforebreak>[\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}][。！？][」）〕]</beforebreak>
<afterbreak>[「『〔]?[\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}]+</afterbreak>
</rule>
<!--newline-->
<rule break="yes">
<beforebreak>\n</beforebreak>
<afterbreak></afterbreak>
</rule>
<rule break="yes">
<beforebreak>。）</beforebreak>
<afterbreak>[\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}]</afterbreak>
</rule>
<rule break="yes">
<beforebreak>[\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}]。</beforebreak>
<afterbreak>（[1-9]）</afterbreak>
</rule>
<rule break="yes">
<beforebreak>[\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}]。』</beforebreak>
<afterbreak>『</afterbreak>
</rule>
</languagerule>
