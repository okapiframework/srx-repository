<!-- 
LanguageTool is published under the GNU Lesser General Public License (LGPL).
For more detailed license information, see COPYING.txt and README.md.
->

<languagerule languagerulename="Persian">
<rule break="no">
<beforebreak>\bwww\.</beforebreak>
<afterbreak>\w</afterbreak>
</rule>
<rule break="no">
<beforebreak>\b(نه|بله)\!\s</beforebreak>
<afterbreak>\p{N}</afterbreak>
</rule>
<rule break="no">
<beforebreak>[\[\(]*…[\]\)]* </beforebreak>
<afterbreak>\p{Ll}</afterbreak>
</rule>
<rule break="no">
<beforebreak>\p{Ps}[!?؟]+\p{Pe} </beforebreak>
<afterbreak></afterbreak>
</rule>
<rule break="no">
<beforebreak>[\.!?؟…]+\p{Pe} </beforebreak>
<afterbreak>\p{Ll}</afterbreak>
</rule>
<rule break="no">
<beforebreak>[«»"”']\s*</beforebreak>
<afterbreak>\s*\p{Ll}</afterbreak>
</rule>
<rule break="no">
<beforebreak>[«'"„][\.!?؟…]['"”»]\s</beforebreak>
<afterbreak></afterbreak>
</rule>
<rule break="no">
<beforebreak>\b\p{L}\.\s</beforebreak>
<afterbreak>\p{L}\.\s</afterbreak>
</rule>
<rule break="no">
<beforebreak>\b\p{L}\.</beforebreak>
<afterbreak>\p{L}\.</afterbreak>
</rule>
<rule break="yes">
<beforebreak>[^,،][\s]\p{L}{2}\.\s</beforebreak>
<afterbreak>\p{N}+\)\s</afterbreak>
</rule>
<rule break="no">
<beforebreak>[\.\s]\p{L}{1,2}\.\s</beforebreak>
<afterbreak>[\p{N}\p{Ll}]</afterbreak>
</rule>
<rule break="no">
<beforebreak>[\[\(]*\.\.\.[\]\)]* </beforebreak>
<afterbreak>[^\p{Lu}]</afterbreak>
</rule>
<rule break="no">
<beforebreak>\b\p{Lu}\.\s\p{Lu}\.\s</beforebreak>
<afterbreak></afterbreak>
</rule>
<rule break="no">
<beforebreak>\b\p{Lu}\.\p{Lu}\.\s</beforebreak>
<afterbreak></afterbreak>
</rule>
<rule break="no">
<beforebreak>[^\.]\s[ضصثقفغعهخحجچشسیبلاتنمکگ\ظطزرذدپوًٌٍَُِّْA-Z]\.\s</beforebreak>
<afterbreak></afterbreak>
</rule>
<rule break="no">
<beforebreak>\(\p{Ll}+\.\s</beforebreak>
<afterbreak></afterbreak>
</rule>
<rule break="yes">
<beforebreak>[\.!?؟…][«»\u00BB\u2019\u201D\u203A"'\p{Pe}\u0002¹²³]*\s</beforebreak>
<afterbreak></afterbreak>
</rule>
<rule break="yes">
<beforebreak>[\.!?؟…][«»'"\u00BB\u2019\u201D\u203A\p{Pe}\u0002]*</beforebreak>
<afterbreak>\p{Lu}[^\p{Lu}]</afterbreak>
</rule>
<rule break="yes">
<beforebreak>\s\p{L}[\.!?؟…]\s</beforebreak>
<afterbreak>\p{Lu}\p{Ll}</afterbreak>
</rule>
</languagerule>
