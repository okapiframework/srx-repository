<languagerule languagerulename="Armenian">
<!--Two initialized names, e.g. "B. H. Roberts", 1-->
<rule break="no">
<beforebreak>\p{Lu}\.</beforebreak>
<afterbreak>\s\p{Lu}\.\s[\p{Lu}$]</afterbreak>
</rule>
<!--Two initialized names, e.g. "B. H. Roberts", 2-->
<rule break="no">
<beforebreak>\p{Lu}\.\s\p{Lu}\.</beforebreak>
<afterbreak>\s\p{Lu}\p{L}+</afterbreak>
</rule>
<!--initialized middle name - e.g. "David O. McKay";also place-names - e.g. "Washington, D. C."-->
<rule break="no">
<beforebreak>\b\p{Lu}\p{L}+,?\s\p{Lu}\.</beforebreak>
<afterbreak>\s</afterbreak>
</rule>
<!--initialized first name - e.g. "L. Tom Perry"-->
<rule break="no">
<beforebreak>\b\p{Lu}\.</beforebreak>
<afterbreak>\s\p{Lu}\p{L}+\s\p{Lu}\p{L}+</afterbreak>
</rule>
<!--ellipsis character-->
<rule break="yes">
<beforebreak>[:։՜?!]\s*\u2026|\u2026\s*[:։՜?!]</beforebreak>
<afterbreak>\s\p{Pi}*\p{Lu}</afterbreak>
</rule>
<!--Regular colon used as a final stop, Armenian final stop or Armenian exclamation mark, and regular exclamation mark and regular question mark, just in case they occur.-->
<rule break="yes">
<beforebreak>[:։՜?!]\p{Pf}?</beforebreak>
<afterbreak>\s[\p{Pi}|"|']*\p{Lu}</afterbreak>
</rule>
<!--ellipsis character w/ punctuation before or after-->
<rule break="yes">
<beforebreak>[:։՜?!]?['"\p{Pf}\p{Pe}]?\s*(\u2026|\.\s*\.\s*\.\s*)|(\u2026|\.\s*\.\s*\.)\s*[:։՜?!]?['"\p{Pf}\p{Pe}]?</beforebreak>
<afterbreak>\s*[\p{Pi}\p{Ps}\(\[\{&lt;]*\p{Lu}</afterbreak>
</rule>
<!--footnote references 1-->
<rule break="yes">
<beforebreak>\p{Ll}{2,}[:։՜?!][\p{Pe}\p{Pf}"']{0,3}[0-9]{1,2}</beforebreak>
<afterbreak>\s\s?\p{Lu}</afterbreak>
</rule>
<!--footnote references 2-->
<rule break="yes">
<beforebreak>\p{Ll}{2,}[\p{Pe}\p{Pf}"']{0,3}[:։՜?!][0-9]{1,2}</beforebreak>
<afterbreak>\s\s?\p{Lu}</afterbreak>
</rule>
<!--ellipsis following terminal punctuation-->
<rule break="yes">
<beforebreak>[:։՜?!][\s\p{Pe}\p{Pf}\]\}\)]*</beforebreak>
<afterbreak>\u2026</afterbreak>
</rule>
<!--ellipsis followed by opening bracket-->
<rule break="yes">
<beforebreak>\u2026</beforebreak>
<afterbreak>\s*[\[\{\(]+\s*</afterbreak>
</rule>
<!--ellipsis followed by closing bracket-->
<rule break="yes">
<beforebreak>\u2026\s*[\]\}\)]+</beforebreak>
<afterbreak>\s*</afterbreak>
</rule>
<!--inline scripture references-->
<rule break="yes">
<beforebreak>[:։՜?!]['"\p{Pe}\p{Pf}]*</beforebreak>
<afterbreak>\s*[\p{Pi}\p{Ps}\[\{\(]([1-5] )?\p{Lu}</afterbreak>
</rule>
<!--copyright statement-->
<rule break="yes">
<beforebreak>[:։՜?!][\p{Pe}\p{Pf}"']*\s*</beforebreak>
<afterbreak>©\s*[0-9]{4}</afterbreak>
</rule>
<rule break="yes">
<beforebreak>[:։՜?!]</beforebreak>
<afterbreak>\p{Lu}</afterbreak>
</rule>
<!--newline-->
<rule break="yes">
<beforebreak>\n</beforebreak>
<afterbreak></afterbreak>
</rule>
</languagerule>
