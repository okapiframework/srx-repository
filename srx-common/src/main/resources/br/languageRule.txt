<!-- 
LanguageTool is published under the GNU Lesser General Public License (LGPL).
For more detailed license information, see COPYING.txt and README.md.
->

<languagerule languagerulename="Breton">
<rule break="no">
<beforebreak>\b[dD]\.l\.e\.?</beforebreak>
<afterbreak></afterbreak>
</rule>
<!-- Break rules -->
<rule break="yes">
<beforebreak>[\.!?…][\u0002|'|"|«|\)|\]|\}¹²³]?\s+</beforebreak>
<afterbreak></afterbreak>
</rule>
<rule break="yes">
<beforebreak>[\.!?…]['"\p{Pe}\u00BB\u201D]?</beforebreak>
<afterbreak>\p{Lu}[^\p{Lu}]</afterbreak>
</rule>
<rule break="yes">
<beforebreak>\s\p{L}[\.!?…]\s</beforebreak>
<afterbreak>\p{Lu}\p{Ll}</afterbreak>
</rule>
</languagerule>
