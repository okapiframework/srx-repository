<!-- 
LanguageTool is published under the GNU Lesser General Public License (LGPL).
For more detailed license information, see COPYING.txt and README.md.
->

<languagerule languagerulename="Default">
<rule break="yes">
<beforebreak>\u2029</beforebreak>
<afterbreak></afterbreak>
</rule>
<!--for smooth interoperability with Anaphraseus translation plugin for OpenOffice.org-->
<rule break="yes">
<beforebreak></beforebreak>
<afterbreak>&lt;0\}</afterbreak>
</rule>
<!--Anaphraseus segment start marker-->
<rule break="yes">
<beforebreak>\{0></beforebreak>
<afterbreak></afterbreak>
</rule>
</languagerule>
